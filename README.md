# Linear Interpolation of Histograms

Perform a linear interpolation between two histograms as a function of a characteristic parameter of the distribution.

The algorithm is described in [Read, A. L., "Linear Interpolation of Histograms", NIM A 425 (1999) 357-360](https://inspirehep.net/literature/501018).

The interpolation of continuous functions is implemented in ROOT class [RooIntegralMorph](https://root.cern/doc/master/classRooIntegralMorph.html).
      
This ROOT-based C++ implementation is based on the FORTRAN77
implementation used by the DELPHI experiment at LEP (d_pvmorph.f).
The use of double precision allows pdf's to be accurately 
interpolated down to something like 10**-15.

For histograms with constant bin width, the binning is also interpolated,
such that the input histograms don't have to be identical. For histograms
with variable bin width the binning of the input histograms should be identical -
the output histogram will have the same binning.

Extrapolation is allowed (a warning is given) but the extrapolation 
is not as well-defined as the interpolation and the results should 
be used with great care.

Data in the underflow and overflow bins are completely ignored. 
They are neither interpolated nor do they contribute to the 
normalization of the histograms.

## Getting started

Download the scripts (`*.C` files). In `ROOT` load the interpolation with, e.g., `.L th1dmorph.C+`. Look at the other scripts for examples of how to use it.

`dhee700film.C` and `dheeInterpolations.C` require internal ATLAS simulation results to run. `dgausfilm.C`, however, 
should work out of the box - it demonstrates the interpolation between two Gaussian distributions.

The codes have been validated on MAC OS with ROOT 6.28/04

## Input arguments

`chname, chtitle` : The ROOT name and title assigned to the interpolated histogram. Defaults for the name and title are "TH1D-interpolated" and "Interpolated histogram", respectively.

`hist1, hist2`    : The two input histograms.

`par1,par`       : The values of the linear parameter that characterises the histograms (e.g. a particle mass).

`parinterp`       : The intermediate value of the linear parameter we wish to interpolate to. 
 
`morphedhistnorm` : The normalization of the interpolated histogram (default is -1). If the normalization is givenas <=0 it is computed as from the linear interpolation between the 2 input histograms, otherwise is it taken from the provided value.
 
`idebug`          : Default is zero, for which no internal information displayed. Values 1 and above increase the verbosity of informational output which may prove helpful to understand errors and pathalogical results.
 
The script returns a pointer (TH1D *) to the interpolated result, a new histogram.

## pre-git developments

### Changes 0.31->0.32 09.06.2023
* Proper set of include files for C++ compilation and newest ROOT version (6.28/04)

### Changes 0.3->0.31 17.07.2011:
* Squashed bug that gave errors for histograms with holes.
* This is version for TH1D, still separate code for TH1F.

### Version 0.3 of ROOT implementation, 08.05.2011

### Changes 0.2->0.3  11.08.2011:
* Include files to make compilation with ACLIC, g++ possible
* Give defaults for arguments, fix misplaced declarations, etc.
* Compilation with g++ also works
* Allow to choose between specified or interpolated normalization
* Correct default debug output

## Author

Alex Read, University of Oslo, Department of Physics
