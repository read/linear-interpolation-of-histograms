#include <TF1.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TPad.h>
#include <TColor.h>
#include <TFile.h>
#include <iostream>
#include <sstream>

using namespace std;

TH1D* th1dmorph(string sname,
		string stitle,
		TH1D *hist1,TH1D *hist2,
		Double_t par1,Double_t par2,Double_t parinterp,
		Double_t morphedhistnorm,
		Int_t idebug);

// Read a pair of mass distributions and interpolate to an intermediate mass
// Show the 4 distributions in the same plot, with an instructive legend.

// Make sure that th1dmorph is available, e.g. ".L th1dmorph.C"

void dheeInterpolations()
{
  gStyle->SetOptStat(000000);
  gStyle->SetOptTitle(false);

  string histname = "histname";
  string histtitle = "Histogram title";

  //
  //------300 GeV ee DH
  //
  TFile *tl = new TFile("hist.514640.MGPy8EG_MET_50_dh_lds_mZp_200_ee.root","READ");
  TFile *tr = new TFile("hist.514648.MGPy8EG_MET_50_dh_lds_mZp_400_ee.root","READ");
  TFile *tm = new TFile("hist.500341.MGPy8EG_MET_50_dh_lds_mZp_300_ee.root","READ");

  TH1D *hleft  = (TH1D*)tl->Get("h_mll");
  TH1D *hright = (TH1D*)tr->Get("h_mll");
  TH1D *hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  TH1D *hi300 = th1dmorph(histname,histtitle
		      ,hleft,hright,200,400,300,0.,0);

  TCanvas *cdh300ee = new TCanvas("cdh300ee","Histogram interpolation",1300,700);
  cdh300ee->Draw();

  hleft->GetXaxis()->SetRangeUser(50.,500.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");

  hi300->Draw("same");
  hi300->SetLineColor(kGreen);

  TLegend* ldh300ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh300ee->SetHeader("ee dh samples","C");
  ldh300ee->AddEntry(hleft,"M=200");
  ldh300ee->AddEntry(hmid,"M=300");
  ldh300ee->AddEntry(hright,"M=400");
  ldh300ee->AddEntry(hi300,"Interpolation (200,400)->300");
  ldh300ee->Draw();

  delete tl;
  delete tr;
  delete tm;

  //
  //------400 GeV ee DH
  //
  tr = new TFile("hist.500349.MGPy8EG_MET_50_dh_lds_mZp_500_ee.root","READ");
  tm = new TFile("hist.514648.MGPy8EG_MET_50_dh_lds_mZp_400_ee.root","READ");
  tl = new TFile("hist.500341.MGPy8EG_MET_50_dh_lds_mZp_300_ee.root","READ");

  hleft  = (TH1D*)tl->Get("h_mll");
  hright = (TH1D*)tr->Get("h_mll");
  hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  histname = "hi400";
  TH1D *hi400 = th1dmorph(histname,histtitle
		      ,hleft,hright,300,500,400,0.,0);

  TCanvas *cdh400ee = new TCanvas("cdh400ee","Histogram interpolation",1300,700);
  cdh400ee->Draw();

  hleft->GetXaxis()->SetRangeUser(50.,700.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");

  hi400->Draw("same");
  hi400->SetLineColor(kGreen);

  TLegend* ldh400ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh400ee->SetHeader("ee dh samples","C");
  ldh400ee->AddEntry(hleft,"M=300");
  ldh400ee->AddEntry(hright,"M=400");
  ldh400ee->AddEntry(hmid,"M=500");
  ldh400ee->AddEntry(hi400,"Interpolation (300,500)->400");
  ldh400ee->Draw();
  
  //
  //------500 GeV ee DH
  //
  tr = new TFile("hist.514656.MGPy8EG_MET_50_dh_lds_mZp_600_ee.root","READ");
  tm = new TFile("hist.500349.MGPy8EG_MET_50_dh_lds_mZp_500_ee.root","READ");
  tl = new TFile("hist.514648.MGPy8EG_MET_50_dh_lds_mZp_400_ee.root","READ");

  hleft  = (TH1D*)tl->Get("h_mll");
  hright = (TH1D*)tr->Get("h_mll");
  hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  histname = "hi500";
  TH1D *hi500 = th1dmorph(histname,histtitle
		      ,hleft,hright,300,500,400,0.,0);

  TCanvas *cdh500ee = new TCanvas("cdh500ee","Histogram interpolation",1300,700);
  cdh500ee->Draw();

  hleft->GetXaxis()->SetRangeUser(100.,800.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");

  hi500->Draw("same");
  hi500->SetLineColor(kGreen);

  TLegend* ldh500ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh500ee->SetHeader("ee dh samples","C");
  ldh500ee->AddEntry(hleft,"M=400");
  ldh500ee->AddEntry(hright,"M=600");
  ldh500ee->AddEntry(hmid,"M=500");
  ldh500ee->AddEntry(hi400,"Interpolation (400,600)->500");
  ldh500ee->Draw();

  //
  //------600 GeV ee DH
  //
  tm = new TFile("hist.514656.MGPy8EG_MET_50_dh_lds_mZp_600_ee.root","READ");
  tl = new TFile("hist.500349.MGPy8EG_MET_50_dh_lds_mZp_500_ee.root","READ");
  tr = new TFile("hist.500361.MGPy8EG_MET_50_dh_lds_mZp_700_ee.root","READ");


  hleft  = (TH1D*)tl->Get("h_mll");
  hright = (TH1D*)tr->Get("h_mll");
  hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  histname = "hi600";
  TH1D *hi600 = th1dmorph(histname,histtitle
		      ,hleft,hright,500,700,600,0.,0);

  TCanvas *cdh600ee = new TCanvas("cdh600ee","Histogram interpolation",1300,700);
  cdh600ee->Draw();

  hleft->GetXaxis()->SetRangeUser(100.,900.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");

  hi600->Draw("same");
  hi600->SetLineColor(kGreen);

  TLegend* ldh600ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh600ee->SetHeader("ee dh samples","C");
  ldh600ee->AddEntry(hleft,"M=500");
  ldh600ee->AddEntry(hright,"M=700");
  ldh600ee->AddEntry(hmid,"M=600");
  ldh600ee->AddEntry(hi600,"Interpolation (500,700)->600");
  ldh600ee->Draw();
  
  //
  //------700 GeV ee DH
  //
  tl = new TFile("hist.514656.MGPy8EG_MET_50_dh_lds_mZp_600_ee.root","READ");
  tm = new TFile("hist.500361.MGPy8EG_MET_50_dh_lds_mZp_700_ee.root","READ");
  tr = new TFile("hist.514664.MGPy8EG_MET_50_dh_lds_mZp_800_ee.root","READ");

  hleft  = (TH1D*)tl->Get("h_mll");
  hright = (TH1D*)tr->Get("h_mll");
  hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  histname = "hi700";
  TH1D *hi700 = th1dmorph(histname,histtitle
		      ,hleft,hright,500,700,600,0.,0);

  TCanvas *cdh700ee = new TCanvas("cdh700ee","Histogram interpolation",1300,700);
  cdh600ee->Draw();

  hleft->GetXaxis()->SetRangeUser(200.,1000.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");

  hi700->Draw("same");
  hi700->SetLineColor(kGreen);

  TLegend* ldh700ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh700ee->SetHeader("ee dh samples","C");
  ldh700ee->AddEntry(hleft,"M=600");
  ldh700ee->AddEntry(hright,"M=800");
  ldh700ee->AddEntry(hmid,"M=700");
  ldh700ee->AddEntry(hi700,"Interpolation (600,800)->700");
  ldh700ee->Draw();
  //
  //------800 GeV ee DH
  //
  tl = new TFile("hist.500361.MGPy8EG_MET_50_dh_lds_mZp_700_ee.root","READ");
  tm = new TFile("hist.514664.MGPy8EG_MET_50_dh_lds_mZp_800_ee.root","READ");
  tr = new TFile("hist.514670.MGPy8EG_MET_50_dh_lds_mZp_900_ee.root","READ");

  hleft  = (TH1D*)tl->Get("h_mll");
  hright = (TH1D*)tr->Get("h_mll");
  hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  histname = "hi800";
  TH1D *hi800 = th1dmorph(histname,histtitle
		      ,hleft,hright,700,900,800,0.,0);

  TCanvas *cdh800ee = new TCanvas("cdh800ee","Histogram interpolation",1300,700);
  cdh800ee->Draw();

  hleft->GetXaxis()->SetRangeUser(200.,1100.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");

  hi800->Draw("same");
  hi800->SetLineColor(kGreen);

  TLegend* ldh800ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh800ee->SetHeader("ee dh samples","C");
  ldh800ee->AddEntry(hleft,"M=700");
  ldh800ee->AddEntry(hright,"M=900");
  ldh800ee->AddEntry(hmid,"M=800");
  ldh800ee->AddEntry(hi700,"Interpolation (700,900)->800");
  ldh800ee->Draw();
  //
  //------900 GeV ee DH
  //
  tl = new TFile("hist.514664.MGPy8EG_MET_50_dh_lds_mZp_800_ee.root","READ");
  tm = new TFile("hist.514670.MGPy8EG_MET_50_dh_lds_mZp_900_ee.root","READ");
  tr = new TFile("hist.500365.MGPy8EG_MET_50_dh_lds_mZp_1000_ee.root","READ");

  hleft  = (TH1D*)tl->Get("h_mll");
  hright = (TH1D*)tr->Get("h_mll");
  hmid   = (TH1D*)tm->Get("h_mll");

  hleft->SetDirectory(0);
  hright->SetDirectory(0);
  hmid->SetDirectory(0);

  tl->Close();
  tm->Close();
  tr->Close();

  hleft->Scale(1/hleft->Integral());
  hright->Scale(1/hright->Integral());
  hmid->Scale(1/hmid->Integral());

  histname = "hi900";
  TH1D *hi900 = th1dmorph(histname,histtitle
		      ,hleft,hright,700,900,800,0.,0);

  TCanvas *cdh900ee = new TCanvas("cdh900ee","Histogram interpolation",1300,700);
  cdh900ee->Draw();

  hleft->GetXaxis()->SetRangeUser(200.,1200.);
  hleft->Draw("h");
  hleft->GetXaxis()->SetTitle("M_{ll} (GeV)");
  hmid->Draw("hsame");
  hmid->SetLineColor(kBlack);
  hright->SetLineColor(kRed);
  hright->Draw("hSAME");  
  hi900->Draw("same");
  hi900->SetLineColor(kGreen);

  TLegend* ldh900ee = new TLegend(0.1,0.7,0.48,0.9);
  ldh900ee->SetHeader("ee dh samples","C");
  ldh900ee->AddEntry(hleft,"M=800");
  ldh900ee->AddEntry(hright,"M=1000");
  ldh900ee->AddEntry(hmid,"M=900");
  ldh900ee->AddEntry(hi700,"Interpolation (800,1000)->900");
  ldh900ee->Draw();

  // Save all the canvases to pdf files
  cdh300ee->SaveAs("interpolation_dh_300_ee.pdf");
  cdh400ee->SaveAs("interpolation_dh_400_ee.pdf");
  cdh500ee->SaveAs("interpolation_dh_500_ee.pdf");
  cdh600ee->SaveAs("interpolation_dh_600_ee.pdf");
  cdh700ee->SaveAs("interpolation_dh_700_ee.pdf");
  cdh800ee->SaveAs("interpolation_dh_800_ee.pdf");
  cdh900ee->SaveAs("interpolation_dh_900_ee.pdf");
}
